'use strict';

import data1 from '../data/level1';
import data2 from '../data/level2';
import data3 from '../data/level3';
var commonmethod = require("../common/index");

module.exports.level1Cart =async function(req, res) {
   
    let carts = [];
    try {
        // assign data to a variable
        var allData = data1;
    
    if(allData){
        
       await allData.carts.forEach( cart => {
        let cartTotal= 0; // initialize total of each cart

        // fetching all cart's item one by one
           cart.items.forEach(item => {
           
             // call searchArticle with article_id and assign to variable
             let article = commonmethod.searchArticle(item.article_id,1);
             cartTotal += article.price * item.quantity; // total calculation
            
           });
           
            // push values into carts array
                carts.push({
                    "id" : cart.id,
                    "total" : cartTotal
                });
             });
            return res.json({status:true,message:'All the cart total',data:carts});
        }else{
            return res.json({status:false,data:"Data Not Found"});
        }
    }
    catch (e) {
      console.log(e);
      return res.json({status:false,data:"Something went wrong"});
    }    
};


module.exports.level2Cart =async function(req, res) {
    let carts = [];
    try {
        // assign data to a variable
        var allData = data2;
        
    if(allData){
        await allData.carts.forEach( cart => {
            let cartTotal= 0; // initialize total of each cart

            // fetching all cart's item one by one
            cart.items.forEach(item => {
                        
                // call searchArticle with article_id and assign to variable
                let article = commonmethod.searchArticle(item.article_id,2);
                cartTotal += article.price * item.quantity; // total calculation
                });

                // call deliveryFees with total values for delivery fees calculation
                let fees = commonmethod.deliveryFees(cartTotal);
                cartTotal += fees.price; // add delivery fees to total
            
                // push values into carts array
                carts.push({
                    "id" : cart.id,
                    "total" : cartTotal
                });
            });
           return res.json({status:true,message:'All the cart total',data:carts});
        } else {
             return res.json({status:false,data:"Data Not Found"});
        }
    }
    catch (e) {
      console.log(e);
      return res.json({status:false,data:"Something went wrong"});
    }    
};

module.exports.level3Cart = async function(req, res) {
    let carts = [];
    try {
        // assign data to a variable
        var allData = data3;
       
   if(allData){
        
       await allData.carts.forEach( cart => {
        let cartTotal= 0; // initialize total of each cart

        // fetching all cart's item one by one
        cart.items.forEach(item => {
        let discount = 0; // initialize discount on each iteration
           
        // call searchArticle with article_id and assign to variable
        let article = commonmethod.searchArticle(item.article_id,3);
        let discountData = commonmethod.discounts(item.article_id); // call discounts with article_id to get discount amount of article
        let temptotal = article.price * item.quantity; // assign total value into temp variable
          
        if(discountData) {
         // check type of discount
            if(discountData.type === 'amount') {
              discount = discountData.value * item.quantity; // calculate discount of type amount
            } else {
              discount = Math.round(temptotal * discountData.value / 100); // calculate discount of type percentage
            }
            }

        cartTotal += temptotal - discount; // total calculation
        });
           
        // call deliveryFees with total values for delivery fees calculation
        let fees = commonmethod.deliveryFees(cartTotal);
        cartTotal += fees.price; // add delivery fees to total    
            
            // push values into carts array
            carts.push({
                "id" : cart.id,
                "total" : cartTotal
            });
        });
        return res.json({status:true,message:'All the cart total',data:carts});
    
        }else{
            return res.json({status:false,data:"Data Not Found"});
         }
    }
    catch (e) {
        console.log(e);
        return res.json({status:false,data:"Something went wrong"});
    }  
};